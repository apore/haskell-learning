module FPKodu3 (

         Player (..),
         showMove, readMove, initPosition, getPlayer,
         Move (), Position,
           -- Kuni siiamaani k�ik eksporditavad nimed tulevad importidest,

         move, allMoves, value
           -- need kolm tulevad siit moodulist.

       ) where

---------------------------------------------------------------------
-- FPKodu3.hs
-- Funktsionaalprogrammeerimise meetod - 3. kodu�lesande lahendus
--
-- Autorid:
--          Ain Uljas   (a30830@math.ut.ee)
--          Kaspar Soer (kaspar@hummuli.ee)
--
--
--          Jarmo Rohtla (a30839@math.ut.ee)
--          J�rgo V�li   (a30850@math.ut.ee)
---------------------------------------------------------------------
       
import Position
import Result
import Random

-- move  sooritab etteantud k�igu antud seisus. Ei pea kontrollima
-- k�igu korrektsust.
move::Move->Position->Position

-- Muudab suvalise malendi asukohta
move (Move malend kust kuhu) pos = changePlayer loppSeisPos
    where
        algPos      = changeState kust Nothing pos
        loppSeisPos = changeState kuhu (Just (Piece (getPlayer pos) malend)) algPos

-- Etturi muutmine teiseks malendiks + liikumine
move (Crown kust kuhu malend) pos = changePlayer loppSeisPos
    where 
        player      = getPlayer pos
        algPos      = changeState kust Nothing pos
        loppSeisPos = changeState kuhu (Just (Piece player malend)) algPos

-- vangerdamine
move (Castling c) pos =
     case player of
        White
             -> changePlayer (changeState (square  (kunn - (clDir c)) 1) (Just (Piece White Rook)) (changeState (square  (rook c) 1) Nothing  (changeState (square  kunn 1) (Just (Piece White King ))(changeState (square 5 1) Nothing (disallowCastling player (Long) (disallowCastling player (Short) pos))))))
        Black
             -> changePlayer (changeState (square  (kunn - (clDir c)) 8) (Just (Piece Black Rook)) (changeState (square  (rook c) 8) Nothing  (changeState (square  kunn 8) (Just (Piece Black King ))(changeState (square 5 8) Nothing (disallowCastling player (Long) (disallowCastling player (Short) pos))))))
        where
            kunn = 5 + 2*(clDir c)
            player = getPlayer pos

-- allMoves  leiab antud seisus (mis sisaldab infot ka selle kohta,
-- kes on k�igul) k�ik malereeglitega lubatud k�igud ja nende sooritamise
-- tulemuseks olevad seisud.
allMoves::Position->[(Move,Position)]
allMoves position = kontroll malendid
    where    
        posEPNone = setEPSquare Nothing position  -- Et iga k�igu jaoks en-passant nulli l�heks
        pos = position
        player = getPlayer pos
        vastane = op player
        vastaseMalendid = getPieces vastane pos
        malendid = getPieces player pos
        
        -- Kontrollimine
        kontroll::[(Piece,Sq)]->[(Move,Position)]
        kontroll []     = []
        kontroll (x:xs) =
            case malend of
                Pawn -> etturEdasi koht 1 ++ etturEdasi koht 2 ++ kontroll xs ++ etturVotab koht (-1) ++ etturVotab koht 1
                Rook -> vankerKaigud koht ++ kontroll xs
                Knight -> ratsuKaigud koht ++ kontroll xs
                Bishop -> odaKaigud koht ++ kontroll xs
                Queen -> lipuKaigud koht ++ kontroll xs
                King -> kuningaKaigud koht ++ vangerdusL koht ++ vangerdusP koht ++ kontroll xs
            where
                malend  = fst x
                koht    = snd x

                

        -- Suhteline rida
        suhtRida::Sq->Int
        suhtRida sq = relRank player (rank sq)
        
        -- Kontroll, kas vastaval positsioonil on ikka vastase malend
        kasVastaseMalend::Sq->Bool
        kasVastaseMalend sq = otsiVastaseMalend vastaseMalendid
            where
                otsiVastaseMalend::[(Piece,Sq)]->Bool
                otsiVastaseMalend [] = False
                otsiVastaseMalend (x:xs)
                    | snd x == sq = True
                    | otherwise = otsiVastaseMalend xs
                    
                    
         -- Kas ruut on tyhi?
        kasTyhi::Sq->Bool
        kasTyhi sq = getState sq pos == Nothing
                        
        -- Etturi �lendamine
        etturYlendamine::Sq->Sq->[(Move,Position)]
        etturYlendamine sq jr = [(mQ,pQ),(mB,pB),(mR,pR),(mK,pK)]
            where        
                -- Muudame etturi vastavaks
                mQ = Crown sq jr Queen
                pQ = move mQ posEPNone
                mB = Crown sq jr Bishop
                pB = move mB posEPNone
                mR = Crown sq jr Rook
                pR = move mR posEPNone
                mK = Crown sq jr Knight
                pK = move mK posEPNone
                
{- VANGERDAMINE -}
        vangerdusL  :: Sq ->[(Move, Position)]
        vangerdusL sq =
            case player of
                White
                       | (getCastling player Short pos) && ((getState (square 6 1) pos) == Nothing ) && ((getState (square 7 1) pos) == Nothing) && not (kasKuningasTules pos White) && not (kasKuningasTules ( move (Move King sq (square 7 1) ) pos ) White)  && not (kasKuningasTules ( move (Move King sq (square 6 1)) pos ) White )-> [(Castling Short, move (Castling Short) pos)]
                       | otherwise -> []
                Black
                       | (getCastling player Short pos) && ((getState (square 6 8) pos) == Nothing ) && ((getState (square 7 8) pos) == Nothing) && not (kasKuningasTules pos Black) && not (kasKuningasTules ( move (Move King sq (square 7 8)) pos ) Black )&& not (kasKuningasTules ( move (Move King sq (square 6 8)) pos ) Black) -> [(Castling Short, move (Castling Short) pos)]
                       | otherwise -> []


        vangerdusP :: Sq ->[(Move, Position)]
        vangerdusP sq =
           case player of
                White
                       | (getCastling player Long pos) && ((getState (square 4 1) pos) == Nothing ) && ((getState (square 3 1) pos) == Nothing) && ((getState (square 2 1) pos) == Nothing) && not (kasKuningasTules pos White) && not (kasKuningasTules ( move (Move King sq (square 4 1)) pos ) White) && not (kasKuningasTules ( move (Move King sq (square 3 1)) pos ) White ) && not (kasKuningasTules ( move (Move King sq (square 2 1)) pos ) White ) -> [(Castling Long, move (Castling Long) pos)]
                       | otherwise -> []
                Black
                       | (getCastling player Long pos) && ((getState (square 4 8) pos) == Nothing ) && ((getState (square 3 8) pos) == Nothing) && ((getState (square 2 8) pos) == Nothing) && not (kasKuningasTules pos Black) && not(kasKuningasTules ( move (Move King sq (square 4 8)) pos ) Black )&& not (kasKuningasTules ( move (Move King sq (square 3 8)) pos ) Black) && not (kasKuningasTules ( move (Move King sq (square 2 8)) pos ) Black)-> [(Castling Long, move (Castling Long) pos)]
                       | otherwise -> []         
        
{- ETTUR EDASI �HE V�RRA + ETTUR EDASI KAHE V�RRA-}        
        -- J�rgmise ruudu koordinaadid vastavalt v�rvusele
        jargmineRuut::Sq->Int->Sq
        jargmineRuut sq palju =
            case player of
                White -> inDirFrom 0 palju sq
                Black -> inDirFrom 0 (-palju) sq
        
        -- Ettur edasi �he v�rra
        etturEdasi::Sq->Int->[(Move,Position)]
        etturEdasi sq palju
            | palju == 2 && suhtRida sq /= 2       = []   -- Kui kaks edasi ning rida pole 2
            | onBoard jr == False                  = []
            | palju == 2 && kasTyhi j_yks == False = []
            | kasTyhi jr == False                  = []
            | kasKuningasTules po player == True   = []
            | suhtRida jr == 8                     = etturYlendamine sq jr
            | otherwise                            = [(mo,po)]
                where
                    mo = Move Pawn sq jr
                    po = if palju==2 then move mo (setEPSquare (Just j_yks) posEPNone) -- M��rame en-passant v�lja
                         else move mo posEPNone
                    jr = jargmineRuut sq palju
                    j_yks = jargmineRuut sq (palju-1)

{- ETTURIGA VASAKULE V�TMINE + PAREMALE V�TMINE -} 
        -- Kas saab paremale/vasakule liikuda, vastavalt v�rvusele
        kasSaabVotta::Sq->Bool
        kasSaabVotta sq
            | onBoard sq == False                   = False
            | getEPSquare pos == Just sq            = True      -- Kui tegemist en-passant v�ljaga, siis v�tmine lubatud
            | kasVastaseMalend sq == False          = False
            | otherwise           = getState sq pos /= Nothing
            
        -- J�rgmine ruut vastavalt sellele, kas v�etakse paremale v�i vasakule
        jargmineVotmiseRuut::Sq->Int->Sq
        jargmineVotmiseRuut sq palju=
            case player of
                White -> inDirFrom (-palju) 1 sq
                Black -> inDirFrom palju (-1) sq
                
        -- Etturiga vasakule v�tmine
        etturVotab::Sq->Int->[(Move,Position)]
        etturVotab sq palju
            | kasSaabVotta jr == False           = []
            | kasKuningasTules po player == True = []
            | suhtRida jr == 8                   = etturYlendamine sq jr
            | otherwise                          = [(mo,po)]
                where
                    mo = Move Pawn sq jr
                    po = move mo posEPNone
                    jr = jargmineVotmiseRuut sq palju
          
{- VANKER: K�IGUD -}
        vankerKaigud::Sq->[(Move,Position) ]
        vankerKaigud sq = vankerKaik 0 1 sq sq ++ vankerKaik 0 (-1) sq sq ++ vankerKaik (-1) 0 sq sq ++ vankerKaik 1 0 sq sq
        
        -- Kas saab liikuda
        kasVankerSaab::Sq->Bool
        kasVankerSaab sq
            | onBoard sq == False                               = False -- Kas v�ljal?
            | kasTyhi sq == True                                = True  -- Kas v�li t�hi?
            | kasVastaseMalend sq == True                       = True  -- Kui vastase malend, siis saame v�tta
            | otherwise                                         = False -- muul juhul ei k�lba
        
        -- Jargmine vankri ruut
        jargmineVankriRuut::Int->Int->Sq->Sq
        jargmineVankriRuut x y sq =
            case player of
                White -> inDirFrom x y sq
                Black -> inDirFrom (-x) (-y) sq
            
        -- Vankri liikumised
        vankerKaik::Int->Int->Sq->Sq->[(Move,Position)]
        vankerKaik x y algOlek sq
            | kasVankerSaab jr == False          = []
            | kasKuningasTules po player == True = []
            | kasVastaseMalend jr == True        = [(mo,po)]
            | otherwise                          = [(mo,po)] ++ vankerKaik x y algOlek jr
                where
                    mo = Move Rook algOlek jr
                    po = move mo posEPNone
                    jr = jargmineVankriRuut x y sq

{- ODA K�IGUD -}
        odaKaigud::Sq->[(Move,Position)]
        odaKaigud sq = odaKaik 1 1 sq sq ++
                       odaKaik (-1) 1 sq sq ++
                       odaKaik (-1) (-1) sq sq ++
                       odaKaik 1 (-1) sq sq
        
        -- Kas oda saab liikuda?
        kasOdaSaab::Sq->Bool
        kasOdaSaab sq
            | onBoard sq == False           = False
            | kasTyhi sq == True            = True
            | kasVastaseMalend sq == True   = True
            | otherwise                     = False
        
        -- J�rgmine oda ruut
        jargmineOdaRuut::Int->Int->Sq->Sq
        jargmineOdaRuut x y sq =
            case player of
                White -> inDirFrom x y sq
                Black -> inDirFrom (-x) (-y) sq 
            
        -- Oda liikumised
        odaKaik::Int->Int->Sq->Sq->[(Move,Position)]
        odaKaik x y algOlek sq
            | kasOdaSaab jr == False             = []
            | kasKuningasTules po player == True = []
            | kasVastaseMalend jr == True        = [(mo,po)]
            | otherwise                          = [(mo,po)] ++ odaKaik x y algOlek jr
                where
                    mo = Move Bishop algOlek jr
                    po = move mo posEPNone
                    jr = jargmineOdaRuut x y sq
                    
{- LIPU K�IGUD -}
        -- Lipu k�igud
        lipuKaigud::Sq->[(Move,Position)]
        lipuKaigud sq = lipuKaik 1 1 sq sq ++ lipuKaik 1 0 sq sq ++
                        lipuKaik 1 (-1) sq sq ++ lipuKaik 0 (-1) sq sq ++
                        lipuKaik (-1) (-1) sq sq ++ lipuKaik (-1) 0 sq sq ++
                        lipuKaik (-1) 1 sq sq ++ lipuKaik 0 1 sq sq 
        
        -- Kas saab liikuda
        kasLippSaab::Sq->Bool
        kasLippSaab sq
            | onBoard sq == False         = False
            | kasTyhi sq == True          = True
            | kasVastaseMalend sq == True = True
            | otherwise                   = False
            
        -- J�rgmine lipu k�ik
        jargmineLipuKaik::Int->Int->Sq->Sq
        jargmineLipuKaik x y sq =
            case player of
                White -> inDirFrom x y sq
                Black -> inDirFrom (-x) (-y) sq                                       
        
        -- Lipu k�ik
        lipuKaik::Int->Int->Sq->Sq->[(Move,Position)]
        lipuKaik x y algOlek sq
            | kasLippSaab jr == False            = []
            | kasKuningasTules po player == True = []
            | kasVastaseMalend jr == True        = [(mo,po)]
            | otherwise                          = [(mo,po)] ++ lipuKaik x y algOlek jr
                where
                    mo = Move Queen algOlek jr
                    po = move mo posEPNone
                    jr = jargmineLipuKaik x y sq
        
{- RATSU K�IGUD -}
        ratsuKaigud::Sq->[(Move,Position)]
        ratsuKaigud sq = ratsuKaik (-2) 1 sq ++ ratsuKaik (-1) 2 sq ++
                         ratsuKaik 1 2 sq ++ ratsuKaik 2 1 sq ++
                         ratsuKaik (-2) (-1) sq ++ ratsuKaik (-1) (-2) sq ++
                         ratsuKaik 1 (-2) sq ++ ratsuKaik 2 (-1) sq
        
        -- Kas saab liikuda
        kasRatsuSaab::Sq->Bool
        kasRatsuSaab sq
            | onBoard sq == False         = False
            | kasTyhi sq == True          = True
            | kasVastaseMalend sq == True = True
            | otherwise                   = False
        
        -- J�rgmine ratsu k�ik
        jargmineRatsuKaik::Int->Int->Sq->Sq
        jargmineRatsuKaik x y sq =
            case player of
                White -> inDirFrom x y sq
                Black -> inDirFrom (-x) (-y) sq
            
        -- Ratsu k�ik
        ratsuKaik::Int->Int->Sq->[(Move,Position)]
        ratsuKaik x y sq
            | kasRatsuSaab jr == False   = []
            | kasKuningasTules po player = []
            | otherwise                  = [(mo,po)]
                where
                    mo = Move Knight sq jr
                    po = move mo posEPNone
                    jr = jargmineRatsuKaik x y sq

{- KUNINGA K�IGUD -}
        -- Kuninga k�igud
        kuningaKaigud::Sq->[(Move,Position)]
        kuningaKaigud sq = kuningaKaik 1 1 sq ++ kuningaKaik 1 0 sq ++
                           kuningaKaik 1 (-1) sq ++ kuningaKaik 0 (-1) sq ++
                           kuningaKaik (-1) (-1) sq ++ kuningaKaik (-1) 0 sq ++
                           kuningaKaik (-1) 1 sq ++ kuningaKaik 0 1 sq 
                           
        -- Kas saab liikuda
        kasKuningasSaab::Sq->Bool
        kasKuningasSaab sq
            | onBoard sq == False         = False
            | kasTyhi sq == True          = True
            | kasVastaseMalend sq == True = True
            | otherwise                   = False
            
        -- J�rgmine kuninga k�ik
        jargmineKuningaKaik::Int->Int->Sq->Sq
        jargmineKuningaKaik x y sq =
            case player of
                White -> inDirFrom x y sq
                Black -> inDirFrom (-x) (-y) sq                                       

        -- Kuninga k�ik
        kuningaKaik::Int->Int->Sq->[(Move,Position)]
        kuningaKaik x y sq
            | kasKuningasSaab jr == False        = []
            | kasKuningasTules po player == True = []
            | otherwise                          = [(mo,po)]
                where
                    mo = Move King sq jr
                    po = move mo posEPNone
                    jr = jargmineKuningaKaik x y sq
{--
 -- Funktsioon, mis kontrollib, kas vastava v�rviga m�ngija kuningas on tules v�i mitte
--}
kasKuningasTules::Position->Player->Bool
kasKuningasTules pos player = kasTules
    where
        malendid        = getPieces player pos
        vastane         = op player
        vastaseMalendid = getPieces vastane pos
        kAsukoht        = otsiKuningas malendid
        
        -- Otsime kuninga yles
        otsiKuningas::[(Piece,Sq)]->Sq
        otsiKuningas (x:xs)
            | fst x == King = snd x
            | otherwise     = otsiKuningas xs
        
        -- Kontrollimine
        kasTules::Bool
        kasTules = otsiEttur (-1) 1 kAsukoht || otsiEttur 1 1 kAsukoht ||
                   otsiRatsu (-2) 1 kAsukoht || otsiRatsu (-1) 2 kAsukoht ||
                   otsiRatsu 1 2 kAsukoht || otsiRatsu 2 1 kAsukoht ||
                   otsiRatsu 2 (-1) kAsukoht || otsiRatsu 1 (-2) kAsukoht ||
                   otsiRatsu (-1) (-2) kAsukoht || otsiRatsu (-2) (-1) kAsukoht ||
                   otsiVanker (-1) 0 kAsukoht || otsiVanker 0 1 kAsukoht ||
                   otsiVanker 1 0 kAsukoht || otsiVanker 0 (-1) kAsukoht ||
                   otsiOda (-1) 1 kAsukoht || otsiOda 1 1 kAsukoht ||
                   otsiOda 1 (-1) kAsukoht || otsiOda (-1) (-1) kAsukoht ||
                   otsiKunn (-1) 0 kAsukoht || otsiKunn (-1) 1 kAsukoht ||
                   otsiKunn 0 1 kAsukoht || otsiKunn 1 1 kAsukoht ||
                   otsiKunn 1 0 kAsukoht || otsiKunn 1 (-1) kAsukoht ||
                   otsiKunn 0 (-1) kAsukoht || otsiKunn (-1) (-1) kAsukoht
        
         -- Kontroll, kas vastaval positsioonil on ikka vastase malend
        kasVastaseMalend::(Piece,Sq)->Bool
        kasVastaseMalend p_sq = otsiVastaseMalend vastaseMalendid
            where
                otsiVastaseMalend::[(Piece,Sq)]->Bool
                otsiVastaseMalend [] = False
                otsiVastaseMalend (x:xs)
                    | p_sq == x = True
                    | otherwise = otsiVastaseMalend xs
                    
         -- Kas ruut on tyhi?
        kasTyhi::Sq->Bool
        kasTyhi sq = getState sq pos == Nothing
        
        -- Kontrollime, kas on ettur sellel positsioonil
        jargmineRuut::Int->Int->Sq->Sq
        jargmineRuut x y sq =
            case player of
                White -> inDirFrom x y sq
                Black -> inDirFrom (-x) (-y) sq
        
        -- Vaatame, et ettureid l�heduses ei oleks
        otsiEttur::Int->Int->Sq->Bool
        otsiEttur x y sq
            | onBoard jr == False = False
            | otherwise           = kasVastaseMalend (Pawn,jr)
            where
                jr = jargmineRuut x y sq
                
        -- Vaatame, et ratsusid l�heduses ei oleks
        otsiRatsu::Int->Int->Sq->Bool
        otsiRatsu x y sq
            | onBoard jr == False = False
            | otherwise           = kasVastaseMalend (Knight,jr)
                where
                    jr = jargmineRuut x y sq
                  
        -- Vaatame, et kuningat l�heduses ei oleks
        otsiKunn::Int->Int->Sq->Bool
        otsiKunn x y sq
            | onBoard jr == False = False
            | otherwise           = kasVastaseMalend (King,jr)
                where
                    jr = jargmineRuut x y sq
                    
        -- Idee selline, et vankri ja oda kontrolliga vaatan ka lipu k�igud �le
        -- Vaatame, et vankreid l�heduses ei oleks
        otsiVanker::Int->Int->Sq->Bool
        otsiVanker x y sq
            | onBoard jr == False = False
            | kasTyhi jr == True  = otsiVanker x y jr
            | otherwise           = kasVastaseMalend (Rook,jr) || kasVastaseMalend (Queen,jr)
                where
                    jr = jargmineRuut x y sq
                    
        -- Vaatame, et oda l�heduses ei oleks
        otsiOda::Int->Int->Sq->Bool
        otsiOda x y sq
            | onBoard jr == False = False
            | kasTyhi jr == True  = otsiOda x y jr
            | otherwise           = kasVastaseMalend (Bishop,jr) || kasVastaseMalend (Queen,jr)
                where
                    jr = jargmineRuut x y sq
                                              
-- value  hindab seisu kasutades moodulis  Result  defineeritud v��rtust��pi 
-- Value a , kus  a  on mingi t��p klassist  RealFrac . P�hiline on, et ta 
-- suudaks m�ngu l�pul aru saada, kumb on v�itnud v�i on tulemuseks viik. 
-- Aga soovitav on lisada ka mingi lihtne heuristik suvalise seisu
-- hindamiseks, kasv�i ainult materjalip�hine, et minimaksalgoritm kiiremini
-- t��taks.

-- Queen = 9
-- Rook = 5
-- Bishop = 3
-- Knight = 3
-- Pawn = 1
-- King = 50 
-- Max = 9 + 10 + 6 + 6 + 8 = 39
value::Position->Value Float
value pos
    | kasKuningasTules pos player && length (allMoves pos) == 0 = Definite Lose 0 -- Kaotus
    | kasKuningasTules pos player && length (allMoves vPos) == 0 = Definite Win 0 -- V�it
    | length (allMoves pos) == 0 = Estimated (0.0) -- Viik
    | length (allMoves vPos) == 0 = Estimated (0.0) -- Viik
    | otherwise = Estimated summa
        where
            player = getPlayer pos
            vPos = changePlayer pos
            vastane = op player
            
            summa = leiaSumma player - leiaSumma vastane
            
            -- Leiame summa
            leiaSumma::Player->Float
            leiaSumma player = s (getPieces player pos)
                where
                    s::[(Piece,Sq)]->Float
                    s [] = 0.0
                    s (x:xs) =
                        case malend of
                            Pawn -> 0.01 + s xs
                            Rook -> 0.05 + s xs
                            Knight -> 0.03 + s xs
                            Bishop -> 0.03 + s xs
                            Queen -> 0.09 + s xs
                            King -> 0.5 + s xs
                        where
                            (malend,_) = x
