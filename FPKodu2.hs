module FPKodu2 where

---------------------------------------------------------------------
-- FPKodu2.hs
-- Funktsionaalprogrammeerimise meetod - 2. kodu�lesannete lahendused
--
-- Autorid:
--          Kaspar Soer (kaspar@hummuli.ee)
--          Ain Uljas (a30830@math.ut.ee)
---------------------------------------------------------------------

-- 1. �lesanne - Funktsioon delAll kontrollib, kas etteantud element esineb listis ja
-- v�ljastab kirje, mille esimene pool on t�ev��rtus ja teine pool sama j�rjend,
-- millest antud element on v�lja korjatud.
 
delAll :: Eq a => a -> [a] -> (Bool,[a])
delAll x xs = foldr f b xs
    where f  y (a, as) 
    		| x==y = (True, as)
    		| otherwise = (a , y: as)
          b = (False , [])

{-HN

Ei t��ta l�pmatul listil.
* 0.8

-}

-- 2. �lesanne - Funktsioon countSigns v�tab argumendiks t�isarvude listi ja v�ljastab
-- kolmev�ljalise kirje, milles loendatakse vastavalt alla nulli, nulle ja �le nulli v��rtusi.

countSigns :: [Int] -> (Int,Int,Int)
countSigns xs = foldl f a xs
    where f (x, y, z) m 
              | compare m 0 == LT = (x+1, y, z)
              | m == 0  = (x, y+1, z)
              | otherwise = (x, y, z+1)
          a = (0, 0, 0)

{-HN

compare m 0  v��rtus annab k�ik informatsiooni k�tte hargnemise jaoks.
Rohkem poleks vaja midagi kontrollida, r��kimata avaldise  compare a 0 == LT
v��rtustamisest.
* 0.95

�ravahetamiseni sarnane Lehiste ja Moori r�hma t��dega.
/ 3

-}		
		
-- 3. �lesanne. Kontrollime, kas tegemist on babababi s�naga
babababi::String->Bool
babababi str
   | str=="A"                = True
   | str==""                 = False
   | odd (length str)        = False
   | babiKontroll str==False = False
   | otherwise               = z
     where
        z = (babababi (reverse v) || babababi (map asendus v)) && babababi u
        u = fst fZip
        v = snd fZip
	fZip = ffZip str
	asendus 'A' = 'B'
	asendus 'B' = 'A'

-- Tagastab paari, kus s�ned vastavalt argumendi s�nele
-- Tagastatud paarides t�hed �le �he erinevates listides
ffZip::String->(String,String)
ffZip []        = ("","")
ffZip (x:y:xs)  = (x:fst xxs,y:snd xxs)
                  where
		      xxs = ffZip xs

-- Kontrollib, kas s�nes on ainult A-d ja B-d
babiKontroll::String->Bool
babiKontroll [] = True
babiKontroll (x:xs)
             | x/='A' && x/='B' = False
             | otherwise        = babiKontroll xs

{-HN

Kole palju on m�ttetut ilmutatud hargnemist. N�iteks  babababi  definitsioon
(ilma  where-osata) on samav��rselt
babababi str = str == 'A' || 
               (null str && even (length str) && babiKontroll str && z)
* 0.95

Funktsiooni  babiKontroll  kutsutakse v�lja igal rekursioonisammul, mis on
m�ttetu, sest kui juba korra on leitud, et s�ne koosneb ainult A-dest ja
B-dest, siis see nii ka j��b.
* 0.95

-}
  

-- 4. �lesanne tudeng saab argumendiks t�isarvu ja v�ljastab tudengi�lesande vastuse
tudeng :: Int -> Integer
tudeng n
    | compare n 0 == LT = error "negatiivne argument!"
    | otherwise = tud [1] n  1

-- akumulaatoriga abifunktsioon, mis tegeliku t�� �ra teeb
tud :: [Integer] -> Int -> Int -> Integer
tud (x:xs) n l
    | n == 0 = x
    | even l  = tud ( ( x + (x:xs) !!  ( div l 2 - 1) ) :x:xs) (n-1) (l+1)
    | otherwise  = tud ( ( x + (x:xs) !! (div l 2)):x:xs) (n-1) (l+1)

{-HN

tudeng2  kahele viimasele valvurile vastavad read saanuks kokku v�tta:
| otherwise = tud (x + (x : xs) !! div (l - 1) 2 : x : xs) (n - 1) (l + 1)
mis viitab, et  l  kohal v�inuks pigem kasutada 1 v�rra v�iksemat arvu.
* 0.95

Kuin�idist olnuks sobiv kasutada.

�ravahetamiseni sarnane Rohtla r�hma t��ga.
/ 2

-}

-- 5. �lesanne. Joonistab etteantud m��tudega ruudustiku
ruudud::Int->Int->Int->String
ruudud col row len = concat(take row (repeat (miinusRuudud col len ++ postidRuududLen col len))) ++ miinusRuudud col len

-- Tagastab miinuste rea vastavalt veergude arvule ning ruudud k�ljepikkusele
miinusRuudud::Int->Int->String
miinusRuudud col len = concat(take col (repeat (' ':take len (repeat '-')))) ++ "\n"

-- Tagastab postide rea vastavalt veergude arvule ning ruudu k�ljepikkusele
postidRuudud::Int->Int->String
postidRuudud col len = "|" ++  concat (take col (repeat (take len (repeat ' ') ++ "|"))) ++ "\n"

-- Tagastab postid vastavalt k�ljepikkusele
postidRuududLen::Int->Int->String
postidRuududLen col len = concat(take len (repeat (postidRuudud col len)))

