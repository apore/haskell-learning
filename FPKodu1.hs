module FPKodu1 where
---------------------------------------------------------------------
-- FPKodu1.hs
-- Funktsionaalprogrammeerimise meetod - 1. kodu�lesannete lahendused
--
-- Autorid:
--          Kaspar Soer (kaspar@hummuli.ee)
--			Ain Uljas (a30830@math.ut.ee)
---------------------------------------------------------------------

-- 1. �lesanne - Funktsioon atMost kontrollib, kas list on mitte pikem kui etteantud arv.
atMost :: Int -> [a] -> Bool
atMost n [] -- kui list on t�hi
	| n < 0 = False
	| otherwise = True

atMost n (_:as) -- kui list on mittet�hi
	| n==0 = False 
	| otherwise = atMost (n-1) as

{-HN

atMost  t��tab ebaefektiivselt, kui arv on negatiivne ja list on pikk.
* 0.6

Esimese deklaratsiooni valvuritega konstruktsioon oleks l�hemalt ja
samav��rselt  = n >= 0 .

-}

-- 2. �lesanne - Stringi reavahesymbolid DOS-i tekstiformaadile vastavaks
toDOS::String->String
toDOS n=concat (map changeChar n)

{-HN

Ehk  toDOS = (>>= changeChar)  :)

-}

-- Funktsioon teisendab '\n' "\r\n"-ks
changeChar::Char->String
changeChar n
	| n=='\n'   = "\r\n"
	| otherwise = [n]


-- 3. �lesanne - Tagastab listi, kus elemendid vastavalt predikaadile
filterNext::(a->Bool)->[a]->[a]
filterNext pred [] = [] 
filterNext pred (a:[]) = [] 				 -- j�rgnevaid elemente pole
filterNext pred (a:as) -- �ldjuht
	| pred a = head as : filterNext pred as  -- kui element rahuldab predikaati, siis panna listi j�rgmine element ja funktsioon ise
	| otherwise = filterNext pred as		 -- listi midagi ei panda, j�tkatakse rekursiooninga


-- 4. �lesanne - Kahe listi liitmine, kus elemendid vaheldumisi ja �le j��v osa pannaks listi l�ppu
flatZip::[a]->[a]->[a]

flatZip [] [] = []
flatZip a [] = a
flatZip [] a = a -- erijuhud
flatZip (a:as) (b:bs) =  a :  b : flatZip as bs -- teeb t��

{-HN

Esimene deklaratsioon on m�ttetu, sest see juht on h�lmatud teise (samuti
kolmanda) deklaratsiooniga.
* 0.95

-}

-- 5. �lesanne - Leiab argumentlisti k�ik ts�klilised nihked
rotations :: [a]->[[a]]
rotations [] = [] -- juhuks kui argument peaks kohe t�hi olema
rotations as = rota (length as) as -- annab juhtimise �le teisele funktsioonile kusjuures kaasa antakse ka listi pikkus

-- rotations funktsiooni abifunktsioon
rota :: Int -> [a] -> [[a]]
rota n (x:xs) 
	| n == 0 = [] -- baasjuht
	| otherwise = [ xs ++ [x] ] ++ rota (n-1) (xs ++ [x]) -- p�hijuht, listi esimene element pannakse l�ppu ja j�tkatakse rekursiivselt

{-HN

xs ++ [x]  arvutatakse igal rekursioonisammul kaks korda.
* 0.95

-}
